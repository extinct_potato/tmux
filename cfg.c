/* $OpenBSD$ */

/*
 * Copyright (c) 2008 Nicholas Marriott <nicholas.marriott@gmail.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF MIND, USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 * OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tmux.h"

struct client		 *cfg_client;
static char		 *cfg_file;
int			  cfg_finished;
static char		**cfg_causes;
static u_int		  cfg_ncauses;
static struct cmdq_item	 *cfg_item;

static enum cmd_retval
cfg_client_done(__unused struct cmdq_item *item, __unused void *data)
{
	if (!cfg_finished)
		return (CMD_RETURN_WAIT);
	return (CMD_RETURN_NORMAL);
}

static enum cmd_retval
cfg_done(__unused struct cmdq_item *item, __unused void *data)
{
	if (cfg_finished)
		return (CMD_RETURN_NORMAL);
	cfg_finished = 1;

	if (!RB_EMPTY(&sessions))
		cfg_show_causes(RB_MIN(sessions, &sessions));

	if (cfg_item != NULL)
		cmdq_continue(cfg_item);

	status_prompt_load_history();

	return (CMD_RETURN_NORMAL);
}

void
set_cfg_file(const char *path)
{
	free(cfg_file);
	cfg_file = xstrdup(path);
}

void
start_cfg(void)
{
	const char	*home;
	int		 flags = 0;
	struct client	*c;

	/*
	 * Configuration files are loaded without a client, so commands are run
	 * in the global queue with item->client NULL.
	 *
	 * However, we must block the initial client (but just the initial
	 * client) so that its command runs after the configuration is loaded.
	 * Because start_cfg() is called so early, we can be sure the client's
	 * command queue is currently empty and our callback will be at the
	 * front - we need to get in before MSG_COMMAND.
	 */
	cfg_client = c = TAILQ_FIRST(&clients);
	if (c != NULL) {
		cfg_item = cmdq_get_callback(cfg_client_done, NULL);
		cmdq_append(c, cfg_item);
	}

	cfg_file == NULL;

	static const char *myopts[] = {
		"set -g prefix C-a",
		"unbind C-b",
		"bind C-a send-prefix",
		"set -g status-interval 1",
		"set -g status-style bg=black,fg=cyan",
		"set -g status-left-style bg=black,fg=green",
		"set -g status-left-length 40",
		"set -g status-left \"#S #[fg=white]» #[fg=yellow]#I #[fg=cyan]#P\"",
		"set -g status-right-style bg=black,fg=cyan",
		"set -g status-right-length 40",
		"set -g status-right \"#H #[fg=white]« #[fg=yellow]%H:%M:%S #[fg=green]%d-%b-%y\"",
		"set -g window-status-format \" #I:#W#F \"",
		"set -g window-status-current-format \" #I:#W#F \"",
		"set -g window-status-current-style bg=red,fg=white",
		"set -g window-status-activity-style bg=black,fg=yellow",
		"set -g window-status-separator \"\"",
		"set -g status-justify centre",
		"set -g pane-border-style bg=default,fg=default",
		"set -g pane-active-border-style bg=default,fg=green",
		"set -g display-panes-colour default",
		"set -g display-panes-active-colour default",
		"set -g clock-mode-colour red",
		"set -g clock-mode-style 24",
		"set -g message-style bg=default,fg=default",
		"set -g message-command-style bg=default,fg=default",
		"set -g mode-style bg=red,fg=default",
		"set -g mouse on",
		"bind \'\"\' split-window -c \"#{pane_current_path}\"",
		"bind % split-window -h -c \"#{pane_current_path}\"",
		"bind c new-window -c \"#{pane_current_path}\"",
	};

	struct cmd_parse_result *pr;

	for(int i = 0; i < nitems(myopts); i++)
	{
		pr = cmd_parse_from_string(myopts[i], NULL);
		if (pr->status != CMD_PARSE_SUCCESS)
		{
			fatalx("bad default key: %s", myopts[i]);
		}
		cmdq_append(NULL, cmdq_get_command(pr->cmdlist, NULL, NULL, 0));
		cmd_list_free(pr->cmdlist);
	}

	cmdq_append(NULL, cmdq_get_callback(cfg_done, NULL));
}

int
load_cfg(const char *path, struct client *c, struct cmdq_item *item, int flags,
    struct cmdq_item **new_item)
{
	return (0);
}

void
cfg_add_cause(const char *fmt, ...)
{
	va_list	 ap;
	char	*msg;

	va_start(ap, fmt);
	xvasprintf(&msg, fmt, ap);
	va_end(ap);

	cfg_ncauses++;
	cfg_causes = xreallocarray(cfg_causes, cfg_ncauses, sizeof *cfg_causes);
	cfg_causes[cfg_ncauses - 1] = msg;
}

void
cfg_print_causes(struct cmdq_item *item)
{
	u_int	 i;

	for (i = 0; i < cfg_ncauses; i++) {
		cmdq_print(item, "%s", cfg_causes[i]);
		free(cfg_causes[i]);
	}

	free(cfg_causes);
	cfg_causes = NULL;
	cfg_ncauses = 0;
}

void
cfg_show_causes(struct session *s)
{
	struct window_pane		*wp;
	struct window_mode_entry	*wme;
	u_int				 i;

	if (s == NULL || cfg_ncauses == 0)
		return;
	wp = s->curw->window->active;

	wme = TAILQ_FIRST(&wp->modes);
	if (wme == NULL || wme->mode != &window_view_mode)
		window_pane_set_mode(wp, &window_view_mode, NULL, NULL);
	for (i = 0; i < cfg_ncauses; i++) {
		window_copy_add(wp, "%s", cfg_causes[i]);
		free(cfg_causes[i]);
	}

	free(cfg_causes);
	cfg_causes = NULL;
	cfg_ncauses = 0;
}
